
$(document).on("click", ".device-choose", function(){
	//console.debug($(this).data("id"));
	
	//console.log(siteurl); // bar
	
	var elem = $(this);
	var html;
	
	$("#id_kategorija").val(elem.data("id"));
	
	$.get("devices/"+$(this).data("id"), function(response){
		
		
		$(".ierices .box-body").fadeOut("fast", function(){
			
			$(this).empty();
			
			var back = '<a href="devices"><div class="col-lg-4 col-xs-7"><div class="small-box bg-blue"><div class="inner"><h3>Atpakaļ</h3><p></p></div><div class="icon"></div></div></div></a>';

			var plus = '<a href="#" class="add-device"><div class="col-lg-4 col-xs-7"><div class="small-box bg-blue"><div class="inner"><h3>Pievienot</h3><p></p></div><div class="icon"><i class="fa fa-plus"></i></div></div></div></a>';
			
			$(".ierices .box-body").append(plus);
			$(".ierices .box-body").append(back);
			
			$(".ierices .box-title").html('<h1>'+elem.data("name")+'</h1>');
			
			$.each(response.specifikacija, function(key, val){
				//$(".box-body").append(val.nosaukums+'<br />');
				
				//console.debug(response);

				html = '<div class="control-group"><label for="'+val.nosaukums+'">'+val.nosaukums+': </label><div class="controls"><input id="'+val.nosaukums+'" class="form-control" placeholder="'+val.nosaukums+'" name="specifikacija['+val.id+']" type="text" value=""></div></div>';
				$(".device-form-bottom").append(html);
				
			});

			if(response.aprikojums.items.ierices){

					var devices;

					$.each(response.aprikojums.items.ierices.items, function(devicekey, device){

						devices ='<div class="col-lg-4 col-xs-7 device-choose" data-id="" data-name=""><div class="small-box bg-blue"><div class="inner"><h3>'+device.nosaukums+'</h3><p><div class="device-count"></div></p></div><div class="icon"><i class="ion ion-pie-graph"></i></div><a class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></div></div>';

							$('.devices').append(devices);
					});

			}
			
			
			
			
		});
		
		//console.debug(response);

		$(".ierices .box-body").fadeIn("fast");
		$(".ierices .device-form").fadeIn("fast");

		$(".ierices .device-choose").fadeOut("fast");
		
	})
	
	return false;
});

$(document).on("click", ".add-device", function(){

	var deviceform = $(".device-form").clone();
	$(".device-form").empty().remove();
			
	$(".ierices .box-body").append(deviceform);
			
	$(".ierices .box-body").fadeIn("fast");
	$(".ierices .device-form").fadeIn("fast");
			
	var send = '<br /><div class="control-group"><div class="controls"><input id="btn-add" class="btn btn-success" type="submit" value="Pievienot"></div></div>';
			
	$(".device-form-bottom").append(send);

});


$(document).on("submit", "#devices-form", function(){
	
	//console.debug( $(this).serialize() );
	
	$(".add-loader").fadeIn("fast");
	
	var request = $.ajax({
		url: "/efakultate/admin/devices",
		method: "POST",
		data: $(this).serialize(),
		dataType: "json",
		success: function(response){
			
			$(".add-loader").fadeOut("fast", function(){
				
				$(".device-added").fadeIn("fast", function(){
					
					$(this).delay(5000).fadeOut("fast");
				});
				
			});
			
			console.debug(response);
			
		},
		error:  function(response){
			
			$(".add-loader").fadeOut("fast", function(){
				
				$(".device-added-error").fadeIn("fast", function(){
					
					$(this).delay(5000).fadeOut("fast");
				});
				
			});
			
		}
		
	});
	
	
	
	/*
	$.post("devices/", $(this).serialize(), function(response){
		
		
	}
	*/
	
	return false;
});