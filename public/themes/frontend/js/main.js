
function login(data){
	
	$.ajax({
		type: "POST",
		url: data.attr("action"),
		//url: '/auth/login',
		data: data.serialize(),
		success: function( login ) {
			console.debug( login );
			
			if(login.success){
				//alert('You are In! :)');
				
				console.debug('You are In! :)');
				
				$(".pieslegties").fadeOut(function(){
					$('#myModal').modal("hide");
					$(".user-panel").fadeIn();
					$(".user-info").html(login.user[0].name);
					
				});
				
			}else{
				
				console.debug('Error :)');
				
				//alert('Error :(');
			}
		}
	});
	
	
}

jQuery(function($) {

	//login();
	
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 10000,
			pause: false
		});
	});

	//Ajax contact
	var form = $('.contact-form');
	form.submit(function () {
		$this = $(this);
		$.post($(this).attr('action'), function(data) {
			$this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
		},'json');
		return false;
	});

	//smooth scroll
	$('.main-nav > li').click(function(event) {
		event.preventDefault();
		var target = $(this).find('>a').prop('hash');
		
		//console.debug(target);
		
		if(target == "#parvaldiba"){
			
			$('html, body').animate({
				scrollTop: $(target).offset().top-120
			}, 500);
			
		}else{
			$('html, body').animate({
				scrollTop: $(target).offset().top
			}, 500);
		}
		
		
		
	});
	
	$('.pieslegties').click(function(event) {
		$('#myModal').modal("toggle");
		$('.modal-content').css({top: "50px"});
	});

	//scrollspy
	$('[data-spy="scroll"]').each(function () {
		
		//console.debug( $(this) );
		
		var $spy = $(this).scrollspy('refresh')
	})

	//PrettyPhoto
	$("a.preview").prettyPhoto({
		social_tools: false
	});

	//Isotope
	$(window).load(function(){
		$portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : 'li',
			layoutMode : 'fitRows'
		});
		$portfolio_selectors = $('.portfolio-filter >li>a');
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});
	
	
	
	$( '#login-form' ).on( 'submit', function(e){
		e.preventDefault(); 
		login($(this));
	});
	
});