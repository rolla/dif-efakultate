-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2015 at 09:44 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `efakultate`
--

-- --------------------------------------------------------

--
-- Table structure for table `aprikojums`
--

CREATE TABLE IF NOT EXISTS `aprikojums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategorija` int(10) unsigned NOT NULL,
  `nosaukums` text COLLATE utf8_unicode_ci NOT NULL,
  `pers_nr` int(11) NOT NULL,
  `inventariz_nr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iepirksanas_gads` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_telpa` int(11) NOT NULL,
  `id_tehnikis` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aprikojums_id_kategorija_foreign` (`id_kategorija`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategorija`
--

CREATE TABLE IF NOT EXISTS `kategorija` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategorija` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `specifikacijas_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `kategorija`
--

INSERT INTO `kategorija` (`id`, `kategorija`, `specifikacijas_ids`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Personālais dators', '3, 4, 5, 6, 7, 8, 13, 9, 10, 11, 12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Portatīvais dators', '3, 4, 5, 6, 7, 8, 13, 9, 10, 11, 12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Monitors', '6, 7, 8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Planšete', '3, 4, 14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'Dokumentu kamera', '15, 16, 6, 7, 8, 17, 14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'Drukas iekārta', '18, 19, 20, 21, 22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 'Projektors', '23, 24, 6, 7, 17, 25, 26, ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'Interaktīvā tāfele', '27, 17, 26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_01_15_105324_create_roles_table', 1),
('2015_01_15_114412_create_role_user_table', 1),
('2015_01_26_115212_create_permissions_table', 1),
('2015_01_26_115523_create_permission_role_table', 1),
('2015_02_09_132439_create_permission_user_table', 1),
('2015_05_05_230455_update_users_table', 2),
('2015_05_06_181259_create_kategorija_table', 3),
('2015_05_06_181844_create_aprikojums_table', 4),
('2015_05_06_183534_create_specifikacija_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE IF NOT EXISTS `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'masteradmin', '', 1, '2015-04-24 08:06:39', '2015-04-24 08:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-04-24 09:54:51', '2015-04-24 09:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `specifikacija`
--

CREATE TABLE IF NOT EXISTS `specifikacija` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nosaukums` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- Dumping data for table `specifikacija`
--

INSERT INTO `specifikacija` (`id`, `nosaukums`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Procesors', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'RAM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'HDD', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'VGA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 'HDMI', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'DVI', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 'Datorpele', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 'Tastatūra', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 'Austiņas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 'Skaļrunis', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 'Videokarte', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 'Atmiņas slots', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 'Fokuss(auto/manual)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 'Uzņemšanas laukums', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 'USB', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 'Drukas veids(tintes/lāzera)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 'Melnbalta druka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 'Krāsaina druka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 'Dokumenta MAX izmērs', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 'Skenēšana(jā/nē)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 'RCA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 'S-VIDEO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 'Audio 3,5 mm', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 'Malu attiecība', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 'OS atbalsts', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Raitis Rolis', 'raitis.rolis@gmail.com', '$2y$10$TVmJr3Kcr1olr9fh.F4Ob.cG1t0QCvVtoB.MYlXu2xZkpHV37JK4K', 'VzhbikzuTlPjpwt9F58K6L7nn6wIFkP8yT929m2fe1yyRigVYv7Mu0DP6RPo', '2015-04-24 09:34:31', '2015-05-06 01:45:10', NULL),
(2, 'Test', 'test@test.lv', '$2y$10$d2Ewoi4kdPTCducEKRHuv.sdznj6ZvZsUc/naoGFTwOQotbfWx1QW', 'kWglWuWIzkjnHYXyfLLwYKYqEhmvDcOjxElsHL0VTaR0F2lhWdjYiOUIjGc1', '2015-05-05 17:03:48', '2015-05-05 17:04:36', NULL),
(3, 'Sveta', 'sveta@test.lv', '$2y$10$q4o/KzyWOPtMzRGIPsmj9OQrVA6qayEisSXVmS/jaoFpRZycfRKz6', 'uF66Cowf8qbvkFaEWMqdgbbyK0EPFukt8aGOk43DTSByNdhew9cuCD1pO3HV', '2015-05-05 17:30:15', '2015-05-05 18:17:11', NULL),
(4, 'Linards Palācis', 'linards@test.lv', '$2y$10$d64IQ6YeuFxjRTqdySU33emaVIgAMSrs8BpxtpAumWz081PUzKCTW', 'zznKiN0dRqPDDvHX9JA89Wei9ZE5r31h6PmoL5bVOC6cjEazAoAyMpsQWlXP', '2015-05-06 00:22:58', '2015-05-06 00:33:32', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aprikojums`
--
ALTER TABLE `aprikojums`
  ADD CONSTRAINT `aprikojums_id_kategorija_foreign` FOREIGN KEY (`id_kategorija`) REFERENCES `kategorija` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
