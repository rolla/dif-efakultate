<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],
	
	'facebook' => [
		'client_id' => '113931452099539',
		'client_secret' => '55e5c75e381dbb26a93a8f6d84f05968',
		//'redirect' => 'http://your-callback-url',
	],
	
	/*
	'twitter' => [
		'client_id' => 'your-github-app-id',
		'client_secret' => 'your-github-app-secret',
		//'redirect' => 'http://your-callback-url',
	],
	*/
	
	'google' => [
		'client_id' => '621540696794-ttnte0kj69qath24rf30ds7sqrrks764.apps.googleusercontent.com',
		'client_secret' => 'YLFCOAldY6sUWrf9QQA1vAEE',
		'redirect' => 'http://dev.xpnstreamer.urdt.lv/oauth2callback',
	],
	
	

];
