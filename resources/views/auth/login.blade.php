@extends('app')
@section('content')

	<div class="login-box login-page">
		<div class="login-logo">
			<a href="{{ url('/') }}"><b>{{$preTitle}}</b></a>
		</div><!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">@lang('login.authorize')
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>@lang('login.whoops')</strong> @lang('login.there_where_errors')<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</p>
			<form method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
				<div class="form-group has-feedback">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('login.email')">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
	
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password" placeholder="@lang('login.password')">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
	
							
				<div class="row">
					<div class="col-xs-8">	  
					<div class="checkbox icheck">
						<label>
						<input type="checkbox" name="remember"> @lang('login.remeber_me')
						</label>
					</div>						
					</div><!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">@lang('login.login')</button>
					</div><!-- /.col -->
				</div>
			</form>
			
			<a class="btn btn-link" href="{{ url('/password/email') }}">@lang('login.forgot_password')</a>
			
		</div><!-- /.login-box-body -->
	</div><!-- /.login-box -->

	<script>
	  $(function () {
		$('input').iCheck({
		  checkboxClass: 'icheckbox_square-blue',
		  radioClass: 'iradio_square-blue',
		  increaseArea: '20%' // optional
		});
	  });
	</script>
	
@endsection
