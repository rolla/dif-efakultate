	<header id="header" role="banner">
		<div class="container">
			<div id="navbar" class="navbar navbar-default liepu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav main-nav">
						<li class="active"><a href="#main-slider"><i class="icon-home"></i></a></li>
						<li><a href="#parvaldiba">Pārvaldība</a></li>
						<li><a href="#aktuali">Aktuāli</a></li>
						<!--<li><a href="#pricing">Pricing</a></li>-->
						<li><a href="#par-mums">Par mums</a></li>
						<li><a href="#kontakti">Kontakti</a></li>
					</ul>
					
					<ul class="nav navbar-nav pull-right pieslegtie">
						<li><a href="{{ url('/admin') }}"><i class="icon-lock"></i></a></li>
						<li><a href="{{ url('/admin') }}">Pieslēgties</a></li>
					</ul>
					
					<div class="user-panel" style="display: none;">
						
						<ul class="nav navbar-nav pull-right">
							<li><a href="/user/profile"><i class="icon-user"></i><span class="user-info"></span></a></li>
							<li><a href="/auth/logout"><i class="icon-lock"></i> Iziet</a></li>
						</ul>
					
					</div>
					
				</div>
				
				
				
			</div>
		</div>
	</header><!--/#header-->