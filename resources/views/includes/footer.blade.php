	
				<!-- footer -->
				
				<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								&copy; 2015 <a target="_blank" href="http://efakultate.klients.urdt.lv/" title="Dabas un inženierzinātņu e-fakultāte">Dabas un inženierzinātņu e-fakultāte</a>. Visas tiesības paturētas.
							</div>
							<div class="col-sm-6">
								<img class="pull-right" src="{{ asset('themes/frontend/images/dizains-dif.png') }}" alt="DIF" title="DIF">
							</div>
						</div>
					</div>
				</footer>
				
				<!-- footer -->