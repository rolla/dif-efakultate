
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="author" content="Rolla">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	
		<title>@yield('pre_title') : @yield('post_title')</title>
	
		<link rel="stylesheet" href="{{ asset('themes/frontend/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('themes/frontend/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('themes/frontend/css/prettyPhoto.css') }}">
		<link rel="stylesheet" href="{{ asset('themes/frontend/css/main.css') }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">
		
		<!--[if lt IE 9]>
		<script src="{{ asset('themes/fontend/js/html5shiv.js') }}"></script>
		<script src="{{ asset('themes/fontend/js/respond.min.js') }}"></script>
		<![endif]-->
		<link rel="shortcut icon" href="{{ asset('themes/frontend/images/ico/favicon.ico') }}">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('themes/frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('themes/frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('themes/frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('themes/frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">