<!doctype html>
<html>
	<head>
	
		@include('includes.head')
		
	</head>
	
	<body data-spy="scroll" data-target="#navbar" data-offset="0">
		<div class="container">
			
			<!--
			<div class="row">
				<div class="top">
					<div class="content">
					
						<div class="col-md-3">
							<div class="logo"><a href="/"><img src="http://www.liepu.lv/css/img/logo-balts-lv.png" alt=""></a></div>
						</div>
						
						<div class="col-md-9">
							<div class="social" style="width: 350px; padding-top:15px; text-align: right">
								<div class="item dr"><a title="Draugiem.lv lapa" href="http://www.draugiem.lv/liepu"> </a></div>
								<div class="item fb"><a title="Facebook.com lapa" href="http://www.facebook.com/pages/Liepaja/Liepaja-University/387919111006"></a></div>
								<div class="item tw"><a title="Twitter.com konts" href="http://twitter.com/liepu"></a></div>
								<div class="item skyp"><a title="Skype profils" href="skype:liepajas.universitate?chat"></a></div>
								<div class="item rss"><a title="RSS padeve" href="http://www.liepu.lv/rss/rss_lv.xml"></a></div>
								<div class="item mail"><a title="Ienākt LiepU e-pastā" href="https://pasts.liepu.lv/src/login.php"></a></div>
								<div class="item sitemap"><a title="Lapas karte" href="http://www.liepu.lv/lv/lapas-karte/"></a></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			-->
			
			@include('includes.header')
			
			<section id="main-slider" class="carousel">
				<div class="carousel-inner">
					<div class="item active">
						<div class="container">
							<div class="carousel-content">
								<h1>Dabas un inženierzinātņu fakultāte izveidota 2013.gada 30.augustā.</h1>
								<p class="lead">Tās priekšvēsture aizsākās 1954. gadā, kad augstskolā darbojās Fizikas un matemātikas katedra, kas 1962. gadā pārtapa par Matemātikas katedru; 1997. gadā - par Matemātikas un informātikas katedru. Šobrīd Dabas un inžnierzinātņu fakultātē strādā matemātikas, fizikas, informācijas tehnoloģiju, vides zinātņu un inženierzinātņu jomu mācībspēki.</p>
							</div>
						</div>
					</div><!--/.item-->
					<div class="item">
						<div class="container">
							<div class="carousel-content">
								<h1>Dabas un inženierzinātņu fakultātes (DIF) mērķis ir</h1>
								<p class="lead">radīt labvēlīgu un radošu vidi dabas un inženierzinātņu studijām un pētījumiem.</p>
							</div>
						</div>
					</div><!--/.item-->
					<div class="item">
						<div class="container">
							<div class="carousel-content">
								<h1>Profesoram Jānim Mencim - 100</h1>
								<p class="lead">I. Imbovicas grāmatas „Dzīves svinētājs Jānis Mencis” prezentācija<br />Ceturdien, 10.aprīli plkst.15.00 Liepājas Universitātes zālē, Lielā ielā notiks 14 I. Imbovicas grāmatas „Dzīves svinētājs Jānis Mencis” („Apgāds Zvaigzne ABC”), prezentācija. Tad - arī Liepājas Universitātes sagatavotās profesora bibliogrāfijas atvēršanas svētkii.<br />Aicināti visi interesenti.</p>
							</div>
						</div>
					</div><!--/.item-->
					<div class="item">
						<div class="container">
							<div class="carousel-content">
								<h1>10. Latvijas Matemātikas konference un 2.starptautiskā zinātniskā konference "Augstas veiktspējas skaitļošana un matemātiskā modelēšana"</h1>
								<p class="lead">11.-12.aprīlī Liepājas Universirtātes Dabas un inženierzinātņu fakultātē; Kr.Valdemāra ielā 4<br />Latvijas Matemātikas konferenci ik pēc diviem gadiem organizē Latvijas Matemātikas biedrība (LMB). Šogad tās organizēšana ir uzticēta Liepājas Universitātei. LMB mērķis atbalstīt un veicināt matemātikas zinātnes un izglītības attīstību Latvijā. Tā apvieno matemātikas zinātniekus, augstskolas mācībspēkus un studentus (vairāk par biedrību skatīt http://www.mathematics.lv).</p>
							</div>
						</div>
					</div><!--/.item-->
				</div><!--/.carousel-inner-->
				<a class="prev" href="#main-slider" data-slide="prev"><i class="icon-angle-left"></i></a>
				<a class="next" href="#main-slider" data-slide="next"><i class="icon-angle-right"></i></a>
			</section><!--/#main-slider-->
			
			<section id="parvaldiba">
				<div class="container">
					<div class="box first">
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-apple icon-md icon-color1"></i>
									<h4>iOS development</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-android icon-md icon-color2"></i>
									<h4>Android development</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-windows icon-md icon-color3"></i>
									<h4>Windows Phone development</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-html5 icon-md icon-color4"></i>
									<h4>Ruby on Rails development</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-css3 icon-md icon-color5"></i>
									<h4>Javascript development</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
							<div class="col-md-4 col-sm-6">
								<div class="center">
									<i class="icon-thumbs-up icon-md icon-color6"></i>
									<h4>Responsive web design</h4>
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
								</div>
							</div><!--/.col-md-4-->
						</div><!--/.row-->
					</div><!--/.box-->
				</div><!--/.container-->
			</section><!--/#services-->
			
			<section id="aktuali">
				<div class="container">
					<div class="box">
						<div class="center gap">
							<h2>Aktuālie jaunumi</h2>
							<p class="lead">Seko līdzi un iesaisties!</p>
						</div><!--/.center-->
						<ul class="portfolio-filter">
							<li><a class="btn btn-primary active" href="#" data-filter="*">All</a></li>
							<li><a class="btn btn-primary" href="#" data-filter=".bootstrap">Bootstrap</a></li>
							<li><a class="btn btn-primary" href="#" data-filter=".html">HTML</a></li>
							<li><a class="btn btn-primary" href="#" data-filter=".wordpress">Wordpress</a></li>
						</ul><!--/#portfolio-filter-->
						<ul class="portfolio-items col-4">
							<li class="portfolio-item apps">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item1.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item1.jpg') }}"><i class="icon-eye-open"></i></a>             
										</div>
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>
								</div>
							</li><!--/.portfolio-item-->
							<li class="portfolio-item joomla bootstrap">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item2.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item2.jpg') }}"><i class="icon-eye-open"></i></a>  
										</div>
									</div> 
									<h5>Lorem ipsum dolor sit amet</h5>         
								</div>
							</li><!--/.portfolio-item-->
							<li class="portfolio-item bootstrap wordpress">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item3.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item3.jpg') }}"><i class="icon-eye-open"></i></a>        
										</div> 
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>          
								</div>           
							</li><!--/.portfolio-item-->
							<li class="portfolio-item joomla wordpress apps">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item4.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item4.jpg') }}"><i class="icon-eye-open"></i></a>          
										</div>   
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>        
								</div>           
							</li><!--/.portfolio-item-->
							<li class="portfolio-item joomla html">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item5.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item5.jpg') }}"><i class="icon-eye-open"></i></a>          
										</div>  
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>  
								</div>       
							</li><!--/.portfolio-item-->
							<li class="portfolio-item wordpress html">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item6.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item6.jpg') }}"><i class="icon-eye-open"></i></a>           
										</div>  
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>         
								</div>           
							</li><!--/.portfolio-item-->
							<li class="portfolio-item joomla html">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item5.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item5.jpg') }}"><i class="icon-eye-open"></i></a>          
										</div>  
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>  
								</div>       
							</li><!--/.portfolio-item-->
							<li class="portfolio-item wordpress html">
								<div class="item-inner">
									<div class="portfolio-image">
										<img src="{{ asset('themes/frontend/images/portfolio/thumb/item6.jpg') }}" alt="">
										<div class="overlay">
											<a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="{{ asset('themes/frontend/images/portfolio/full/item6.jpg') }}"><i class="icon-eye-open"></i></a>           
										</div>   
									</div>
									<h5>Lorem ipsum dolor sit amet</h5>        
								</div>         
							</li><!--/.portfolio-item-->
						</ul>   
					</div><!--/.box-->
				</div><!--/.container-->
			</section><!--/#portfolio-->
			
			<!--
			<section id="pricing">
				<div class="container">
					<div class="box">
						<div class="center">
							<h2>See our Pricings</h2>
							<p class="lead">Pellentesque habitant morbi tristique senectus et netus et <br>malesuada fames ac turpis egestas.</p>
						</div><!--/.center-->   
						<!--<div class="big-gap"></div>
						<div id="pricing-table" class="row">
							<div class="col-sm-4">
								<ul class="plan">
									<li class="plan-name">Basic</li>
									<li class="plan-price">$29</li>
									<li>5GB Storage</li>
									<li>1GB RAM</li>
									<li>400GB Bandwidth</li>
									<li>10 Email Address</li>
									<li>Forum Support</li>
									<li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
								</ul>
							</div><!--/.col-sm-4-->
							<!--<div class="col-sm-4">
								<ul class="plan featured">
									<li class="plan-name">Standard</li>
									<li class="plan-price">$49</li>
									<li>10GB Storage</li>
									<li>2GB RAM</li>
									<li>1TB Bandwidth</li>
									<li>100 Email Address</li>
									<li>Forum Support</li>
									<li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
								</ul>
							</div><!--/.col-sm-4-->
							<!--<div class="col-sm-4">
								<ul class="plan">
									<li class="plan-name">Advanced</li>
									<li class="plan-price">$199</li>
									<li>30GB Storage</li>
									<li>5GB RAM</li>
									<li>5TB Bandwidth</li>
									<li>1000 Email Address</li>
									<li>Forum Support</li>
									<li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
								</ul>
							</div><!--/.col-sm-4-->
						<!--</div> 
					</div> 
				</div>
			</section><!--/#pricing-->
			
			<section id="par-mums">
				<div class="container">
					<div class="box">
						<div class="center">
							<h2>Mūsu komanda</h2>
							<p class="lead">...</p>
						</div>
						<div class="gap"></div>
						<div id="team-scroller" class="carousel scale">
							<div class="carousel-inner">
								<div class="item active">
									<div class="row">
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team1-new.jpg') }}" alt="" ></p>
												<h3>Anita Jansone<small class="designation">Dekāne</small></h3>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team2-new.jpg') }}" alt="" ></p>
												<h3>Dzintars Tomsons<small class="designation">Lektors</small></h3>
											</div>
										</div>        
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team3-new.jpg') }}" alt="" ></p>
												<h3>Laimdota Eglina<small class="designation">Metodiķe</small></h3>
											</div>
										</div>
									</div>
								</div>
								<!--
								<div class="item">
									<div class="row">
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team3.jpg') }}" alt="" ></p>
												<h3>David Robbins<small class="designation">Co-Founder</small></h3>
											</div>
										</div>   
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team1-new.jpg') }}" alt="" ></p>
												<h3>Philip Mejia<small class="designation">Marketing Manager</small></h3>
											</div>
										</div>     
										<div class="col-sm-4">
											<div class="member">
												<p><img class="img-responsive img-thumbnail img-circle" src="{{ asset('themes/frontend/images/team2.jpg') }}" alt="" ></p>
												<h3>Charles Erickson<small class="designation">Support Manager</small></h3>
											</div>
										</div>
									</div>
								</div>
								-->
							</div>
							<a class="left-arrow" href="#team-scroller" data-slide="prev">
								<i class="icon-angle-left icon-4x"></i>
							</a>
							<a class="right-arrow" href="#team-scroller" data-slide="next">
								<i class="icon-angle-right icon-4x"></i>
							</a>
						</div><!--/.carousel-->
					</div><!--/.box-->
				</div><!--/.container-->
			</section><!--/#about-us-->
		
			<section id="kontakti">
				<div class="container">
					<div class="box last">
						<div class="row">
							<div class="col-sm-6">
								<h1>Sazinies</h1>
								<p>Ir kas sakāms?</p>
								<div class="status alert alert-success" style="display: none"></div>
								<form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php" role="form">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" required="required" placeholder="Vārds">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" required="required" placeholder="Epasts">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Ziņojums"></textarea>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-danger btn-lg">Nosūtīt</button>
											</div>
										</div>
									</div>
								</form>
							</div><!--/.col-sm-6-->
							<div class="col-sm-6">
								<h1>Mūsu adrese</h1>
								<div class="row">
									<div class="col-md-6">
										<address>
											<strong>Dabas un inženierzinātņu fakultāte</strong><br>
											Kr. Valdemāra iela 4.<br>
											Liepāja<br>
											<abbr title="Phone">Tel:</abbr> 634 54046
										</address>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
								<h1>Esi ar mums</h1>
								<div class="row">
									<div class="col-md-6">
										<ul class="social">
											<li><a href="#"><i class="icon-facebook icon-social"></i> Facebook</a></li>
											<li><a href="#"><i class="icon-google-plus icon-social"></i> Google Plus</a></li>
											<li><a href="#"><i class="icon-pinterest icon-social"></i> Pinterest</a></li>
										</ul>
									</div>
									<div class="col-md-6">
										<ul class="social">
											<li><a href="#"><i class="icon-linkedin icon-social"></i> Linkedin</a></li>
											<li><a href="#"><i class="icon-twitter icon-social"></i> Twitter</a></li>
											<li><a href="#"><i class="icon-youtube icon-social"></i> Youtube</a></li>
										</ul>
									</div>
								</div>
							</div><!--/.col-sm-6-->
						</div><!--/.row-->
					</div><!--/.box-->
				</div><!--/.container-->
			</section><!--/#contact-->
			
			<div id="main" class="row">
			
				@yield('content')
				
			</div>
			
			@include('includes.footer')
		
		</div>
		
			@include('includes.foot')
			
			<!-- Modal -->
		<div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<br>
					<div class="bs-example bs-example-tabs">
						<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a href="#signin" data-toggle="tab">Pieslēgties</a></li>
						<li class=""><a href="#signup" data-toggle="tab">Reģistrēties</a></li>
						<li class=""><a href="#why" data-toggle="tab">Kādēļ?</a></li>
						</ul>
					</div>
				<div class="modal-body">
					<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade in" id="why">
					<p>Jo tas ir cool.</p>
					<p></p><br>Sazinies, ja jautājumi</p>
					</div>
					<div class="tab-pane fade active in" id="signin">
					
						<!-- <form id="login-form" class="form-horizontal" method="post" action="{{ asset('/login') }}"> -->
						<fieldset>
						
						{!! Form::open( array(
							'url' => 'auth',
							'id' => 'login-form'
						) ) !!}
						
						<div class="control-group">
							{!! Form::label( 'email', 'epasts:' ) !!}
							<div class="controls">
							{!! Form::text( 'email', '', array(
								'id' => 'email',
								'class' => 'form-control',
								'placeholder' => 'demo',
								'maxlength' => 100,
								'required' => true,
							) ) !!}
							</div>
						</div>
						
						<div class="control-group">
							{!! Form::label( 'password', 'Parole:' ) !!}
							<div class="controls">
								{!! Form::password( 'password', array(
									'id' => 'password',
									'class' => 'form-control',
									'placeholder' => '********',
									'maxlength' => 30,
									'required' => true,
								) ) !!}
							</div>
						</div>
						
						<div class="control-group">
							<div class="controls">
							{!! Form::submit( 'Pieslēgties', array(
								'id' => 'btn-login',
								'class' => 'btn btn-success'
							) ) !!}
							</div>
						</div>
						
						{!! Form::close() !!}
						
						<!--
						<div class="control-group">
						<label class="control-label" for="userid">Lietotājvārds:</label>
						<div class="controls">
							<input required="" id="userid" name="username" type="text" class="form-control" placeholder="demo" class="input-medium" required="">
						</div>
						</div>
			
						
						<div class="control-group">
						<label class="control-label" for="passwordinput">Parole:</label>
						<div class="controls">
							<input required="" id="passwordinput" name="password" class="form-control" type="password" placeholder="********" class="input-medium">
						</div>
						</div>
			
						
						<div class="control-group">
						<label class="control-label" for="rememberme"></label>
						<div class="controls">
							<label class="checkbox inline" for="rememberme-0">
							<input type="checkbox" name="rememberme" id="rememberme-0" value="true">
							Atcerēties mani
							</label>
						</div>
						</div>
			
						
						<div class="control-group">
						<label class="control-label" for="signin"></label>
						<div class="controls">
							<button id="signin" name="signin" class="btn btn-success">Pieslēgties</button>
						</div>
						</div>
						</fieldset>
						</form>
						-->
						
					</div>
					<div class="tab-pane fade" id="signup">
						<form class="form-horizontal">
						<fieldset>
						<!-- Sign Up Form -->
						<!-- Text input-->
						<div class="control-group">
						<label class="control-label" for="Email">Epasts:</label>
						<div class="controls">
							<input id="Email" name="Email" class="form-control" type="text" placeholder="demo@liepu.lv" class="input-large" required="">
						</div>
						</div>
						
						<!-- Text input-->
						<div class="control-group">
						<label class="control-label" for="userid">Lietotājvārds:</label>
						<div class="controls">
							<input id="userid" name="userid" class="form-control" type="text" placeholder="demo" class="input-large" required="">
						</div>
						</div>
						
						<!-- Password input-->
						<div class="control-group">
						<label class="control-label" for="password">Parole:</label>
						<div class="controls">
							<input id="password" name="password" class="form-control" type="password" placeholder="********" class="input-large" required="">
							<em>3-16 Simboli</em>
						</div>
						</div>
						
						<!-- Text input-->
						<div class="control-group">
						<label class="control-label" for="reenterpassword">Atkārtoti paroli:</label>
						<div class="controls">
							<input id="reenterpassword" class="form-control" name="reenterpassword" type="password" placeholder="********" class="input-large" required="">
						</div>
						</div>
						
						<!-- Multiple Radios (inline) -->
						<br>
						<div class="control-group">
						<label class="control-label" for="humancheck">Cilvēka pārbaude:</label>
						<div class="controls">
							<label class="radio inline" for="humancheck-0">
							<input type="radio" name="humancheck" id="humancheck-0" value="robot" checked="checked">Esmu robots</label>
							<label class="radio inline" for="humancheck-1">
							<input type="radio" name="humancheck" id="humancheck-1" value="human">Esmu cilvēks</label>
						</div>
						</div>
						
						<!-- Button -->
						<div class="control-group">
						<label class="control-label" for="confirmsignup"></label>
						<div class="controls">
							<button id="confirmsignup" name="confirmsignup" class="btn btn-success">Reģistrēties</button>
						</div>
						</div>
						</fieldset>
						</form>
				</div>
				</div>
				</div>
				<div class="modal-footer">
				<center>
					<button type="button" class="btn btn-default" data-dismiss="modal">Aizvērt</button>
					</center>
				</div>
				</div>
			</div>
		</div>
			
	</body>
</html>