@extends('admin.app')
@section('content')

<!-- Main row -->
<div class="row">
	<!-- Left col -->
	<section class="col-lg-9 connectedSortable">
		
		<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Lietotāji</h3>
                  <div class="box-tools">
                    <div class="input-group">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody><tr>
                      <th>ID</th>
                      <th>User</th>
					  <th>Login</th>
                      <th>Date</th>
                      <th>Role</th>
                    </tr>
					
					@foreach ($users as $user)
                   <tr>
                      <td><a href="{{ url('/user/'.$user->id.'/edit')}}">{{ $user->id }}</a></td>
                      <td>{{ $user->name }}</td>
                      <!--<td><span class="label label-success">Approved</span></td>-->
					  <td><a href="{{ url('/user/'.$user->id.'/edit')}}">{{ $user->email }}</a></td>
					  <td>{{ $user->created_at->diffForHumans() }}</td>
                      <td>
					@if ($user->is('masteradmin'))
						Master Admin
					@else
						Admin
					@endif
					  </td>
                    </tr>
					
					@endforeach
					
                  </tbody></table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
		
	</section><!-- /.Left col -->
	<!-- right col (We are only adding the ID to make the widgets sortable)-->
	<section class="col-lg-2 connectedSortable">
		
	</section><!-- right col -->
</div><!-- /.row (main row) -->

	<div class="row">
		<section class="col-lg-9 connectedSortable">
			
			<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Reģistrēt Lietotāju</h3>
                  <div class="box-tools">
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                 @if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
		</section>
	</div>

@endsection