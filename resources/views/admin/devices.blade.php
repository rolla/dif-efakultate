@extends('admin.app')
@section('content')

<!-- Main row -->
<div class="row">

	<section class="col-lg-8 connectedSortable device-added" style="display: none; position: fixed;  z-index: 999;">
	<div class="box box-solid box-success">
		<div class="box-header">
		<h3 class="box-title">Ierīce pievienota</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		
		</div><!-- /.box-body -->
	</div>
	</section>
	
	<section class="col-lg-8 connectedSortable device-added-error" style="display: none; position: fixed;  z-index: 999;">
	<div class="box box-solid box-danger">
		<div class="box-header">
		<h3 class="box-title">Ierīce netika pievienota</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		
		</div><!-- /.box-body -->
	</div>
	</section>

	<!-- Left col -->
	<section class="col-lg-12 connectedSortable ierices">
		<div class="col-xs-12">
			  <div class="box">
				<div class="box-header">
				  <h3 class="box-title">Ierīču Kategorijas</h3>
				  <div class="box-tools">
					<!--
					<div class="input-group">
					  <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">
					  <div class="input-group-btn">
						<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
					  </div>
					</div>
					-->
				  </div>
				</div><!-- /.box-header -->
				<div class="box-body ierices table-responsive">
				
				
				
				@foreach ($kategorijas as $kategorija)
				
		<div class="col-lg-4 col-xs-7 device-choose" data-id="{{ $kategorija->id }}" data-name="{{ $kategorija->kategorija }}">
		<div class="small-box bg-{{ $bg_colors[array_rand($bg_colors)] }}">
			<div class="inner">
				<h3>{{ $kategorija->kategorija }}</h3>
				<p>
					<div class="device-count">

					@foreach ($aprikojumu_skaits as $aprikojums)

						@if ( $aprikojums->id_kategorija == $kategorija->id)

    						@if ( $aprikojums->count )
    							{{ $aprikojums->count }}
    						@else
    							0
    						@endif

						@else
    						0
						@endif

					@endforeach
					</div>
				</p>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
		</div><!-- ./col -->
				
				<!--
				   <tr>
					  <td><a href="{{ url('/device/category/'.$kategorija->id.'/edit')}}">{{ $kategorija->id }}</a></td>
					  <td>{{ $kategorija->kategorija }}</td>
					  <!--<td><span class="label label-success">Approved</span></td>
					  <td><a href="{{ url('/device/category/'.$kategorija->id.'/edit')}}">{{ $kategorija->email }}</a></td>
					  <td>{{ $kategorija->created_at->diffForHumans() }}</td>
					  <td>
					
					  </td>
					</tr>
					-->
					
					@endforeach
				
				
				
	
				</div><!-- /.box-body -->
				
				<div class="overlay add-loader" style="display: none;">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
				
			  </div><!-- /.box -->
			</div>
			
	</section><!-- /.Left col -->
</div><!-- /.row (main row) -->

<div class="row">
	<section class="col-lg-12 connectedSortable device-form" style="display: none;">
	
		{!! Form::open( array(
			'url' => 'admin/devices',
			'id' => 'devices-form'
		) ) !!}
		
	<div class="device-form-top">
		
		<div class="control-group">
			<div class="controls">
			{!! Form::submit( 'Pievienot', array(
				'id' => 'btn-add',
				'class' => 'btn btn-success'
			) ) !!}
			</div>
		</div>
		

		{!! Form::hidden( 'aprikojums[id_kategorija]', '', array(
			'required' => true,
			'id' => 'id_kategorija',
		) ) !!}

		{!! Form::hidden( 'aprikojums[id_telpa]', '1', array() ) !!}
		
		{!! Form::hidden( 'aprikojums[id_tehnikis]', '1', array() ) !!}
		
		<div class="control-group">
			{!! Form::label( 'nosaukums', 'Nosaukums:' ) !!}
			<div class="controls">
				{!! Form::text( 'aprikojums[nosaukums]', '', array(
					'id' => 'nosaukums',
					'class' => 'form-control',
					'placeholder' => 'nosaukums',
					'required' => true,
				) ) !!}
			</div>
		</div>
		
		<div class="control-group">
			{!! Form::label( 'pers_nr', 'Personiskais nr:' ) !!}
			<div class="controls">
				{!! Form::text( 'aprikojums[pers_nr]', '', array(
					'id' => 'pers_nr',
					'class' => 'form-control',
					'placeholder' => 'Personiskais nr',
					'required' => true,
				) ) !!}
			</div>
		</div>
		
		<div class="control-group">
			{!! Form::label( 'inventariz_nr', 'Inventarizācijas nr:' ) !!}
			<div class="controls">
				{!! Form::text( 'aprikojums[inventariz_nr]', '', array(
					'id' => 'inventariz_nr',
					'class' => 'form-control',
					'placeholder' => 'Inventarizācijas nr',
					'required' => true,
				) ) !!}
			</div>
		</div>
		
		<div class="control-group">
			{!! Form::label( 'iepirksanas_gads', 'Iepirkšanas gads' ) !!}
			<div class="controls">
				{!! Form::text( 'aprikojums[iepirksanas_gads]', '', array(
					'id' => 'iepirksanas_gads',
					'class' => 'form-control',
					'placeholder' => 'Iepirkšanas gads',
					'required' => false,
				) ) !!}
			</div>
		</div>
		
		</div>
		
		<br /><br />
		
		<div class="device-form-bottom">
		
		
		</div>
		
		{!! Form::close() !!}
		
	</section>
</div><!-- /.row (main row) -->

<div class="row">
	<div class="devices"></div>
</div>

<div class="row">
	<section class="col-lg-8 connectedSortable device-added" style="display: none; position: fixed;  z-index: 999;">
	<div class="box box-solid box-success">
		<div class="box-header">
		<h3 class="box-title">Ierīce pievienota</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		
		</div><!-- /.box-body -->
	</div>
	</section>
	
	<section class="col-lg-8 connectedSortable device-added-error" style="display: none; position: fixed;  z-index: 999;">
	<div class="box box-solid box-danger">
		<div class="box-header">
		<h3 class="box-title">Ierīce netika pievienota</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		
		</div><!-- /.box-body -->
	</div>
	</section>
</div>

@endsection