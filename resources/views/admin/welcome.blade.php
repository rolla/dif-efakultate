@extends('admin.app')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
	<!--
	<div class="col-lg-3 col-xs-6">
		
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>150</h3>
				<p>New Orders</p>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
	<!--
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-green">
			<div class="inner">
				<h3>53<sup style="font-size: 20px">%</sup></h3>
				<p>Bounce Rate</p>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
	
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<a href="{{ url('/admin/users') }}" class="small-box-footer">
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3>{{ $userCount }}</h3>
				<p>@lang('admin.users')</p>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<span class="small-box-footer">@lang('admin.more_info') <i class="fa fa-arrow-circle-right"></i></span>
		</div>
		</a>
	</div><!-- ./col -->
	
	<div class="col-lg-3 col-xs-6">
		<a href="{{ url('/admin/devices') }}" class="small-box-footer">
		<div class="small-box bg-red">
			<div class="inner">
				<h3>-</h3>
				<p>@lang('admin.device_manager')</p>
			</div>
			<div class="icon">
				<i class="ion ion-gear-a"></i>
			</div>
			<span class="small-box-footer">@lang('admin.more_info') <i class="fa fa-arrow-circle-right"></i></span>
		</div>
		</a>
	</div><!-- ./col -->
	
	<div class="col-lg-3 col-xs-6">
		<a href="{{ url('/translations') }}" class="small-box-footer">
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>-</h3>
				<p>@lang('admin.translates')</p>
			</div>
			<div class="icon">
				<i class="ion ion-earth"></i>
			</div>
			<span class="small-box-footer">@lang('admin.more_info') <i class="fa fa-arrow-circle-right"></i></span>
		</div>
		</a>
		
	</div><!-- ./col -->
	
</div><!-- /.row -->

@endsection