
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="author" content="Rolla">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	
		<title>@yield('pre_title') : @yield('post_title')</title>
	
		<link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-theme.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">