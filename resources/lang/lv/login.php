<?php

return array (
  'authorize' => 'Autorizējies',
  'whoops' => 'Aij, aij!',
  'there_where_errors' => 'Notikusi kāda kļūda autorizējoties.',
  'remeber_me' => 'Atcerēties mani',
  'login' => 'Pieslēgties',
  'forgot_password' => 'Aizmirsi paroli?',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'custom-message',
    ),
  ),
  'these_credentials' => 'Šādu edresi neatradām mūsu datubāzē!',
  'incorrect_password' => 'Nepareiza parole!',
  'email' => 'Edrese',
  'password' => 'Parole',
);
