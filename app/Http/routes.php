<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('install', 'Install\InstallController@index');

//Route::get('test', 'UserController@create');

//Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

//Route::get('user_login', 'HomeController@index');

//Route::get('auth', 'HomeController@index');

//Route::get('auth/register', 'Admin\AdminController@check');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

Route::get('/', function(){
    return View::make('pages.sakums');
});

/*
Route::get('/sakums', function(){
    return View::make('pages.sakums');
});

Route::get('bojajumi', function(){
    return View::make('pages.bojajumi');
});

Route::get('biblioteka', function(){
    return View::make('pages.biblioteka');
});

Route::get('parvaldit', function(){
    return View::make('pages.parvaldit');
});

Route::get('registracija', function(){
    //return View::make('pages.registracija');
	return View::make('pages.sakums');
});

Route::get('par-dif', function(){
    //return View::make('pages.pardif');
	return View::make('pages.sakums');
});
*/

Route::get('admin', 'Admin\AdminController@dashboard');
Route::get('admin/users', 'Admin\AdminController@users');

Route::resource('user', 'User\UserController');
Route::resource('admin/devices', 'Admin\DevicesController');

//Route::post('auth/login', ['as' => 'auth', 'uses' => 'Auth\AuthController']);

//Route::post('user', ['as' => 'user_login', 'uses' => 'UserController@login']);
