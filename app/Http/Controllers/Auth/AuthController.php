<?php namespace difLiepu\Http\Controllers\Auth;

use difLiepu\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use difLiepu\User;
use Input;
use Auth;
use Request;
use Validator;
use Lang;

class AuthController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;
	
	protected $redirectPath = '/admin';
	
	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage(){
		return Lang::get('login.these_credentials');
	}
	
	protected function getFailedLoginPasswordMessage(){
		return Lang::get('login.incorrect_password');
	}

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		
		//return response()->json($auth);
		//exit;
		
		//response()->json(
		//dd($auth);

		$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		/*
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);
		*/
		
		/*
		$validator = Validator::make($request, [
			'email' => 'required|email', 'password' => 'required',
		]);
		*/
		
		//dd( $this->redirectPath() );
		
		$credentials = Request::only('email', 'password');
		
		$user = User::where('email', '=', $credentials['email'])->first();
		
		//dd($user);
		
		if(empty($user)){
			
			return redirect($this->loginPath())
					->withInput(Request::only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
			
		}else{
		
			if ($this->auth->attempt($credentials, Request::has('remember')))
			{
				return redirect()->intended($this->redirectPath());
			}
			
			//dd( redirect($this->loginPath())->withInput(Request::only('email', 'remember'))->withErrors(['email' => $this->getFailedLoginMessage(),]) );
		
			return redirect($this->loginPath())
					->withInput(Request::only('email', 'remember'))
					->withErrors([
						'password' => $this->getFailedLoginPasswordMessage(),
					]);
					
		}
		
	}
	
	
	/*
	public function postLogin(){
		
		$input = Input::all();
		
		$attempt = Auth::attempt([
			'email' => $input['email'],
			'password' => $input['password']
		]);
		
		if( $attempt ){
			
			if ( Request::ajax() ){
				//return response('Unauthorized.', 401);
				
				$response = ['success' => true, 'user' => $user = User::where('email', '=', $input['email'])->get()];
				//return 'ajax call';
			}else{
				return redirect('admin');
				//return redirect()->guest('auth/login');
			}
			
		}else{
			
			if ( Request::ajax() ){
				//return response('Unauthorized.', 401);
				
				$response = ['error' => true];
				//return 'ajax call';
			}else{
				return redirect('auth/login')->withInput();
				//return redirect()->guest('auth/login');
			}
			
			
		}
		
		return response()->json( $response );
		
	}
	*/

}
