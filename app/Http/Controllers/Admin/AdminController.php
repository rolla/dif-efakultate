<?php namespace difLiepu\Http\Controllers\Admin;

use difLiepu\Http\Controllers\Controller;

use Bican\Roles\Models\Role;
use \difLiepu\User;
use Auth;
use View;

//use Illuminate\Contracts\View\View;

class AdminController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		//dd( $this->middleware('auth') );
		/*
		if( Auth::guest() ){
			return View::make('auth.login');
		}else{
			return true;
		}
		*/
	}
	
	public function dashboard(){
		//dd( !Auth::guest() );
		if( Auth::check() ){
			$user = User::all();
			//dd( $user );
			$data = array('pageTitle' => 'Administrators', 'userCount' => count($user));
			return View::make('admin.welcome', $data);
		}else{
			return View::make('auth.login');
		}
	}
	
	public function users(){
		if( Auth::check() ){
			$user = User::all();
			$data = array('pageTitle' => 'Administrators Lietotāji', 'userCount' => count($user), 'users' => $user);
			return View::make('admin.users', $data);
		}else{
			return View::make('auth.login');
		}
	}
	
}
