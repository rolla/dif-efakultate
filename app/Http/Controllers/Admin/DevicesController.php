<?php namespace difLiepu\Http\Controllers\Admin;

use difLiepu\Http\Controllers\Controller;

use Bican\Roles\Models\Role;
use difLiepu\User;
use difLiepu\Kategorija;
use Auth;
use View;
use DB;
use JavaScript;
use Request;
use difLiepu\Aprikojums;
use Lang;

//use Illuminate\Contracts\View\View;

class DevicesController extends Controller {
	
	public function __construct(){

	}

	// Module model
	public function sectionsCountRelation(){
    	return $this->hasOne('difLiepu\Aprikojums')->selectRaw('id_kategorija, count(*) as count')->groupBy('id_kategorija');
    	// replace module_id with appropriate foreign key if needed
	}

	// but there is a bit sugar to make it easier (and that s why I renamed it to sectionsCountRelation)

	public function getSectionsCountAttribute(){
    	return $this->sectionsCountRelation->count;
	}

	

	public function index(){
		if( Auth::check() ){
			
			// then you can access it like this:

			//$modules = Aprikojums::with('sectionsCountRelation')->get();
			//$modules->first()->sectionsCountRelation->count;

			// now you can simply do this on every module:
			//$modules->first()->sectionsCount;

			$aprikojumu_skaits = DB::table('aprikojums')
                     ->select(DB::raw('id_kategorija, count(*) as count'))
                     //->where('status', '<>', 1)
                     ->groupBy('id_kategorija')
                     ->get();

                     //selectRaw('id_kategorija, count(*) as count')->groupBy('id_kategorija')

			//dd($aprikojumu_skaits);

			$kategorijas = Kategorija::all();
			
			$bg_colors = array('blue', 'red', 'yellow', 'green', 'danger', 'warning', 'info');
			
			//dd( $kategorijas );
			
			//$specifikacija = DB::table('specifikacija')->whereIn('id', [3, 4, 5, 6, 7, 8, 13, 9, 10, 11, 12])->get();
					
			//dd($specifikacija);
			
			/*
			JavaScript::put([
				'siteurl' => 'bar',
				//'user' => User::first(),
				'age' => 29
			]);
			*/
			
			//SELECT id_kategorija, COUNT(*) as count FROM `aprikojums` GROUP BY id_kategorija

			$data = array('pageTitle' => 'Administrators Ierīces', 'kategorijas' => $kategorijas, 'bg_colors' => $bg_colors, 'aprikojumu_skaits' => $aprikojumu_skaits);
			return View::make('admin.devices', $data);
		}else{
			return View::make('auth.login');
		}
	}
	
	public function roles(){
        return $this->hasMany('difLiepu\Kategorija', 'id');
    }

	public function show($id){
		if( Auth::check() ){
			
			$kategorija = Kategorija::find($id);
			
			//echo($kategorija->specifikacijas_ids);
			

			$devices_arr = Aprikojums::where('id_kategorija', $kategorija->id)->get();

			if( !$devices_arr->isEmpty() ){

				//dd($devices_arr);

				$aprikojums['items']['ierices']['status'] = 'ok';
				$aprikojums['items']['ierices']['items'] = $devices_arr;


			}else{

				$aprikojums['items']['ierices']['status'] = 'info';
				$aprikojums['items']['ierices']['msg'] = Lang::get('messages.devices_not_found');
			}

			
			// SELECT * FROM aprikojums ap LEFT JOIN specifikacija sp ON ap.id=sp.id_aprikojums WHERE id_kategorija = 1
			$aprikojums_arr = Aprikojums::leftJoin('specifikacija', function($join) {
				$join->on('aprikojums.id', '=', 'specifikacija.id_aprikojums');
				})
				->where('aprikojums.id_kategorija', $kategorija->id)->get();


				/*->first([
					'aprikojums.id',
				]);
				*/
				//dd();

			if( !$aprikojums_arr->isEmpty() ){
				$aprikojums['items']['specifikacijas']['status'] = 'ok';
				$aprikojums['items']['specifikacijas']['items'] = $aprikojums_arr;
			}else{
				$aprikojums['items']['specifikacijas']['status'] = 'info';
				$aprikojums['items']['specifikacijas']['msg'] = Lang::get('messages.devices_not_found');
			}
				
			//$c = Aprikojums::find($id);

			//$c = $this->hasMany('Kategorija', 'id');

			//return response()->json( $aprikojums );
			$data['aprikojums'] = $aprikojums;
			
			
			$ids =  explode(', ', $kategorija->specifikacijas_ids);
			
			$specifikacija = DB::table('specifikacijas_ids')->whereIn('id', $ids)->get();
			
			//dd($specifikacija);

			$data['specifikacija'] = $specifikacija;
			
			return response()->json( $data );
			
			
		}else{
			return response()->json( ['error'=>'auth'] );
		}
	}
	
	public function store(){
		if( Auth::check() ){
			$input = Request::except('_token');
			
			//dd( $input['specifikacija'] );
			
			$input['aprikojums']['created_at'] = date('Y-m-d H:i:s');
			
			//dd( $input['aprikojums'] );
			
			
			
			$last_id = DB::table('aprikojums')->insertGetId($input['aprikojums']);
			
			$last_ids['aprikojums'] = $last_id;
			//DB::table('specifikacija')->insert($data);
			
			if($last_id){
				$data;
				
				foreach($input['specifikacija'] as $key => $val){
					
					//echo $key.' -> '.$val;
					if( !empty($val) ){
						$data[] = array('id_aprikojums' => $last_id, 'id_specifikacija' => $key, 'vertiba' => $val, 'created_at' => date('Y-m-d H:i:s'));
					}
					
				}
				
				//dd( $data );
				
				if( !empty($data) ){
					$last_id = DB::table('specifikacija')->insert( $data );
					if($last_id){
						$last_ids['specifikacija'] = $last_id;
					}
				}
			
			}
			
			return response()->json( $last_ids );
			
		}else{
			return response()->json( ['error'=>'auth'] );
		}
	}
	
}
