<?php namespace difLiepu\Http\Controllers\Install;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use difLiepu\Http\Controllers\Controller;

class InstallController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	//Schema Builder
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//return view('welcome');
		
		if($this->check()){
			$this->install();
		}
	}

	
	public function check(){
		
		return true;
	}
	
	public function install(){
		
		/*
		\Schema::create('users', function($table){
			$table->increments('id');
			$table->string('name', '255');
			$table->string('surname', '255');
			$table->string('email', '255');
			$table->longText('description');
			$table->string('username', '255');
			$table->longText('password');
			$table->string('alias', '255');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			
		});
		
		\Schema::create('news', function($table){
			$table->increments('id');
			$table->string('name', '255');
			$table->string('surname', '255');
			$table->longText('text');
			$table->string('alias', '255');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			
		});
		
		\Schema::create('tehniki', function($table){
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->string('vards', '255');
			$table->string('uzvards', '255');
			$table->string('epasts', '255');
			$table->integer('id_darba_laiks');
			$table->integer('id_telpa');
			//$table->string('alias', '255');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			
		});
		
		\Schema::create('telpa', function($table){
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('id_auditorijas_tips');
			$table->string('nosaukums', '255');
			$table->integer('numurs');
			$table->integer('vietu_skaits');
			$table->integer('datoru_skaits');
			$table->integer('id_tehnikis')->unsigned();
			$table->foreign('id_tehnikis')->references('id')->on('tehniki')->onDelete('cascade');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			
			
			
		});
		*/
		
		
		\Schema::create('aprikojums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_kategorija')->unsigned();
			$table->foreign('id_kategorija')->references('id')->on('kategorija');
			$table->text('nosaukums');
			$table->integer('pers_nr');
			$table->string('inventariz_nr');
			$table->timestamp('iepirksanas_gads');
			$table->integer('id_telpa');
			$table->integer('id_tehnikis');
			$table->timestamps();
			$table->softDeletes();
		});
		
		
		echo 'Datu Bāzes Tabulas Izveidotas!';
	}
}


//new $ctrl = InstallController();

