<?php namespace difLiepu\Providers;

use Illuminate\Support\ServiceProvider;
use JavaScript;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		
		view()->share('preTitle', 'LiepU Dabas un inženierzinātņu e-fakultāte');
		view()->share('titleSeperator', ' : ');
		view()->share('preCopy', '&copy; 2015');
		view()->share('postCopy', 'visas tiesības paturētas.');
		view()->share('version', '0.01');
		
		
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'difLiepu\Services\Registrar'
		);
	}

}
