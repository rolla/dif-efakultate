<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SpecifikacijasIDSTableSeeder extends Seeder{
	
	public function run(){
		
		DB::table('specifikacijas_ids')->delete();
		
		$names = array(
			'Procesors', 
			'RAM', 
			'HDD', 
			'VGA', 
			'HDMI', 
			'DVI', 
			'Datorpele', 
			'Tastatūra', 
			'Austiņas', 
			'Skalruņi', 
			'Videokarte', 
			'Atmiņas slots', 
			'Fokuss (auto / manual)', 
			'Uzņemšanas laukums', 
			'USB', 
			'Drukas veids (tintes / lazera)', 
			'Melnbalta druka', 
			'Krāsaina druka', 
			'Dokumenta MAX izmers', 
			'Skenēšana (jā / ne)', 
			'RCA', 
			'S-VIDEO', 
			'Audio 3,5 mm', 
			'Malu attiecība', 
			'OS atbalsts'
			);
		
		
		foreach($names as $name){
			$specs[] = array('nosaukums' => $name, 'alias' => str_slug($name, '-'), 'created_at' => date('Y-m-d H:i:s'));
		}
		
		DB::table('specifikacijas_ids')->insert($specs);
	}
	
}