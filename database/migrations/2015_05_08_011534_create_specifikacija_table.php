<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecifikacijaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('specifikacija', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_aprikojums')->unsigned();
			$table->foreign('id_aprikojums')->references('id')->on('aprikojums')->onDelete('cascade');
			$table->integer('id_specifikacija')->unsigned();
			$table->foreign('id_specifikacija')->references('id')->on('specifikacijas_ids')->onDelete('cascade');
			$table->text('vertiba');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('specifikacija');
	}

}
