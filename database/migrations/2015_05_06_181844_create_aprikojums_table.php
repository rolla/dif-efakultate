<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprikojumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('aprikojums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_kategorija')->unsigned();
			$table->foreign('id_kategorija')->references('id')->on('kategorija');
			$table->text('nosaukums');
			$table->integer('pers_nr');
			$table->string('inventariz_nr');
			$table->timestamp('iepirksanas_gads');
			$table->integer('id_telpa');
			$table->integer('id_tehnikis');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('aprikojums');
	}

}
